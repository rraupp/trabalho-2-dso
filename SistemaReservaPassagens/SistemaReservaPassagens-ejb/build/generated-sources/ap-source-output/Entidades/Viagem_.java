package Entidades;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-05-10T23:35:19")
@StaticMetamodel(Viagem.class)
public class Viagem_ { 

    public static volatile SingularAttribute<Viagem, String> origem;
    public static volatile SingularAttribute<Viagem, Date> dataPartida;
    public static volatile SingularAttribute<Viagem, Integer> id;
    public static volatile SingularAttribute<Viagem, String> destino;
    public static volatile SingularAttribute<Viagem, Date> horaPartida;
    public static volatile SingularAttribute<Viagem, Integer> assentosDisponiveis;

}