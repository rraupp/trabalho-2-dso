/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Raupp
 */
@Entity
@Table(name = "VIAGEM")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Viagem.findAll", query = "SELECT v FROM Viagem v")
    , @NamedQuery(name = "Viagem.findById", query = "SELECT v FROM Viagem v WHERE v.id = :id")
    , @NamedQuery(name = "Viagem.findByOrigem", query = "SELECT v FROM Viagem v WHERE v.origem = :origem")
    , @NamedQuery(name = "Viagem.findByDestino", query = "SELECT v FROM Viagem v WHERE v.destino = :destino")
    , @NamedQuery(name = "Viagem.findByDataPartida", query = "SELECT v FROM Viagem v WHERE v.dataPartida = :dataPartida")
    , @NamedQuery(name = "Viagem.findByHoraPartida", query = "SELECT v FROM Viagem v WHERE v.horaPartida = :horaPartida")
    , @NamedQuery(name = "Viagem.findByAssentosDisponiveis", query = "SELECT v FROM Viagem v WHERE v.assentosDisponiveis = :assentosDisponiveis")
    , @NamedQuery(name = "Viagem.findViagensDisponiveis", query = "SELECT v FROM Viagem v WHERE v.assentosDisponiveis > 0 AND v.dataPartida >= :dataHoje")
})
public class Viagem implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "ORIGEM")
    private String origem;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "DESTINO")
    private String destino;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DATA_PARTIDA")
    @Temporal(TemporalType.DATE)
    private Date dataPartida;
    @Basic(optional = false)
    @NotNull
    @Column(name = "HORA_PARTIDA")
    @Temporal(TemporalType.TIME)
    private Date horaPartida;
    @Column(name = "ASSENTOS_DISPONIVEIS")
    private Integer assentosDisponiveis;

    public Viagem() {
    }

    public Viagem(Integer id) {
        this.id = id;
    }

    public Viagem(Integer id, String origem, String destino, Date dataPartida, Date horaPartida) {
        this.id = id;
        this.origem = origem;
        this.destino = destino;
        this.dataPartida = dataPartida;
        this.horaPartida = horaPartida;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public Date getDataPartida() {
        return dataPartida;
    }

    public void setDataPartida(Date dataPartida) {
        this.dataPartida = dataPartida;
    }

    public Date getHoraPartida() {
        return horaPartida;
    }

    public void setHoraPartida(Date horaPartida) {
        this.horaPartida = horaPartida;
    }

    public Integer getAssentosDisponiveis() {
        return assentosDisponiveis;
    }

    public void setAssentosDisponiveis(Integer assentosDisponiveis) {
        this.assentosDisponiveis = assentosDisponiveis;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Viagem)) {
            return false;
        }
        Viagem other = (Viagem) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidades.Viagem[ id=" + id + " ]";
    }

}
