package SessionBeans;

import Entidades.Cliente;
import Entidades.Passagem;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@LocalBean
public class PassagemFachada {
    
    @PersistenceContext (name = "SistemaReservaPassagens-ejbPU") 
    EntityManager em;
    
    public void persist(Object object) {
        em.persist(object);
    }
    
    public List<Passagem> getListaPassagens () {
        Query query = em.createNamedQuery("Passagem.passagensNaoReservadas");
        return query.getResultList();
    }

    public Integer getUltimoId() {
        Query query = em.createNamedQuery("Passagem.findMaiorId");
        return (Integer) query.getSingleResult();
    }

    public List<Passagem> getReservasCliente(Cliente cliente) {
        Query query = em.createNamedQuery("Passagem.findPassagensDeCliente").setParameter("cliente", cliente);
        return (List<Passagem>) query.getResultList();
    }

    public void cancelarReserva(Integer id) {
        Query query = em.createNativeQuery("DELETE FROM Passagem p WHERE p.id = " + id);
        query.executeUpdate();
    }

}
