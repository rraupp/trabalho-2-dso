package SessionBeans;

import Entidades.Cliente;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@LocalBean
public class ClienteFachada {

    @PersistenceContext(name = "SistemaReservaPassagens-ejbPU")
    EntityManager em;
    
    private Cliente cliente;

    public void persist(Object object) {
        em.persist(object);
    }

    // Metodo que retorna a lista de clientes armazenada na tabela Clientes
    public List<Cliente> getListaClientes() {
        Query query = em.createNamedQuery("Cliente.findAll");
        return (List<Cliente>) query.getResultList();
    }

    public Cliente getClientePorId(Integer id) {
        Query query = em.createNamedQuery("Cliente.findById").setParameter("id", id);
        return (Cliente) query.getSingleResult();
    }

    public Cliente getClientePorEmail(String emailInput) {
        Query query = em.createNamedQuery("Cliente.findByEmail").setParameter("email", emailInput);
        return (Cliente) query.getSingleResult();
    }

    public Integer getUltimoId() {
        Query query = em.createNamedQuery("Cliente.findMaiorId");
        return (Integer) query.getSingleResult();
    }

    public Cliente getClienteParaLogin(String email, String senha) {
        Query query = em.createNamedQuery("Cliente.findClienteLogin")
                .setParameter("email", email)
                .setParameter("senha", senha);
        cliente = null;
        try {
            cliente = (Cliente) query.getSingleResult();
        } catch (NoResultException e) {

        }
        return cliente;
    }

    public Cliente getCliente() {
        return cliente;
    }
    
}
