package SessionBeans;

import Entidades.Viagem;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@LocalBean
public class ViagemFachada {

    @PersistenceContext(name = "SistemaReservaPassagens-ejbPU")
    EntityManager em;
    
    public void persist(Object object) {
        em.persist(object);
    }

    public List<List> getViagensDisponiveis() {
        Date agora = new Date();
        Query query = em.createNamedQuery("Viagem.findViagensDisponiveis").setParameter("dataHoje", agora);
        return query.getResultList();
    }

    public void reservaAssento(Viagem viagem) {
        viagem.setAssentosDisponiveis(viagem.getAssentosDisponiveis()-1);
        em.merge(viagem);
        em.flush();
    }

    public void liberaAssento(Integer id) {
        Query query = em.createNamedQuery("Viagem.findById").setParameter("id", id);
        Viagem v = (Viagem) query.getSingleResult();
        v.setAssentosDisponiveis(v.getAssentosDisponiveis() + 1);
        em.merge(v);
        em.flush();
    }
}