package Entidades;

import Entidades.Passagem;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-05-10T20:02:13")
@StaticMetamodel(Viagem.class)
public class Viagem_ { 

    public static volatile SingularAttribute<Viagem, String> origem;
    public static volatile SingularAttribute<Viagem, Date> dataPartida;
    public static volatile SingularAttribute<Viagem, Integer> id;
    public static volatile SingularAttribute<Viagem, String> destino;
    public static volatile CollectionAttribute<Viagem, Passagem> passagemCollection;
    public static volatile SingularAttribute<Viagem, Date> horaPartida;

}