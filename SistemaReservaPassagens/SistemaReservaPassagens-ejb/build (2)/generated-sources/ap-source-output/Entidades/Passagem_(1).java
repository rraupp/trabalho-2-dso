package Entidades;

import Entidades.Cliente;
import Entidades.Viagem;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-05-10T20:02:13")
@StaticMetamodel(Passagem.class)
public class Passagem_ { 

    public static volatile SingularAttribute<Passagem, Integer> numeroAssento;
    public static volatile SingularAttribute<Passagem, Viagem> viagem;
    public static volatile SingularAttribute<Passagem, Cliente> cliente;
    public static volatile SingularAttribute<Passagem, Integer> id;

}