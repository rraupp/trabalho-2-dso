package beansGerenciadosJSF;

import Entidades.Cliente;
import Entidades.Passagem;
import Entidades.Viagem;
import SessionBeans.PassagemFachada;
import SessionBeans.ViagemFachada;
import java.util.List;
import java.util.Random;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

@Named(value = "reservaViagemBean")
@RequestScoped
public class ReservaViagemBean {

    @EJB
    private ViagemFachada viagemFachada;
    @EJB
    private PassagemFachada passagemFachada;

    private Viagem viagemSelecionada;

    public ReservaViagemBean() {
        viagemSelecionada = new Viagem();
    }

    public Viagem getViagemSelecionada() {
        return viagemSelecionada;
    }

    public void setViagemSelecionada(Viagem viagemSelecionada) {
        this.viagemSelecionada = viagemSelecionada;
    }

    public String abrePaginaReservar() {
        return "/index";
    }

    public List<List> getViagensDisponiveis() {
        return viagemFachada.getViagensDisponiveis();
    }

    public void reservaPassagem(Cliente cliente) {
        Random rand = new Random();
        int idPassagem = rand.nextInt(50000) + 1;
        Passagem pass = new Passagem();
        pass.setId(idPassagem);
        pass.setCliente(cliente);
        pass.setViagem(viagemSelecionada);
        try {
            passagemFachada.persist(pass);
            viagemFachada.reservaAssento(viagemSelecionada);
            
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Passagem reservada!", "Sucesso!"));
        } catch (NullPointerException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Selecione uma viagem!", "Erro!"));
        } catch (EJBException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Tente novamente", "Erro!"));
        }
        //RequestContext context = RequestContext.getCurrentInstance();
        //context.update();
        //return null;
    }

}
