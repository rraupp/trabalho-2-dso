package beansGerenciadosJSF;

import Entidades.Cliente;
import SessionBeans.ClienteFachada;
import java.io.Serializable;
import java.util.List;
import java.util.Random;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

@Named(value = "loginBean")
@SessionScoped
public class LoginBean implements Serializable{

    @EJB
    private ClienteFachada clienteFachada;
    private Cliente cliente;

    public LoginBean() {
        cliente = new Cliente();
    }

    public List<Cliente> getListaClientes() {
        return clienteFachada.getListaClientes();
    }

    public Cliente getCliente() {
        return cliente;
    }

    public String efetuarLogin() {
        cliente = clienteFachada.getClienteParaLogin(cliente.getEmail(), cliente.getSenha());
        if (cliente == null) {
            //TODO mostra mensagem de erro: cliente nao encontrado
            cliente = new Cliente();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cliente não encontrado!", "Erro no Login!"));
            System.out.print("Cliente nao encontrado");
            return "/login";
        } else {
            //realiza login
            return "/cliente";

        }

    }

    public String cadastrarCliente() {
        if (validaCliente()) {
            try {
                Random rand = new Random();
                int id = rand.nextInt(500000) + 1;
                //ultimoId++;
                cliente.setId(id);
                clienteFachada.persist(cliente);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cliente cadastrado!", "Sucesso!"));
                return "/login";
            } catch (Exception e) {
                cadastrarCliente();
            }
        }
        return "/login";
    }

    private boolean validaCliente() {
        if (cliente.getNome().length() >= 6) {
            if (validaEmail()) {
                if (cliente.getTelefone().length() >= 9) {
                    if (cliente.getEndereco().length() > 9) {
                        return true;
                    } else {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Endereco invalido", "Erro!"));
                    }
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Telefone invalido", "Erro!"));
                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Email invalido", "Erro!"));
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nome invalido", "Erro!"));
        }
        return false;
    }

    public boolean validaEmail() {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(cliente.getEmail());
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

    public String efetuarLogout() {
        return "/login";
    }

}
