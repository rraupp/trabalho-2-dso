package beansGerenciadosJSF;

import Entidades.Cliente;
import Entidades.Passagem;
import SessionBeans.PassagemFachada;
import SessionBeans.ViagemFachada;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

@Named(value = "passagensReservadasBean")
@RequestScoped
public class PassagensReservadasBean {

    @EJB
    private ViagemFachada viagemFachada;
    @EJB
    private PassagemFachada passagemFachada;

    private Passagem passagemSelecionada;

    public PassagensReservadasBean() {
        passagemSelecionada = null;
    }

    public PassagemFachada getPassagemFachada() {
        return passagemFachada;
    }

    public List<Passagem> getListaPassagens() {
        return passagemFachada.getListaPassagens();
    }

    public Passagem getPassagemSelecionada() {
        return passagemSelecionada;
    }

    public void setPassagemSelecionada(Passagem passagemSelecionada) {
        this.passagemSelecionada = passagemSelecionada;
    }

    public List<Passagem> getReservasCliente(Cliente cliente) {
        return passagemFachada.getReservasCliente(cliente);
    }

    public void cancelarReserva() {
        try {
            passagemFachada.cancelarReserva(passagemSelecionada.getId());
            viagemFachada.liberaAssento(passagemSelecionada.getViagem().getId());
        } catch (NullPointerException e) {
            
        }
    }

}
